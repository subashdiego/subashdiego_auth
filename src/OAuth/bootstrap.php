<?php

/**
 * Bootstrap the library.
 * Very First file to load All Autoload []
 * @author Subash Diego <subash.diego@gmail.com>
 */

namespace OAuth;

require_once __DIR__ . '/Common/SD_auto_loader.php';

$autoloader = new Common\SD_auto_loader(__NAMESPACE__, dirname(__DIR__));

$autoloader->register();
